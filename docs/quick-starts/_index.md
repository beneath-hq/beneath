---
menu:
  docs:
    identifier: quick-starts
    name: Quick Starts
    weight: 200
weight: 200

title: Quick starts
description: Tutorials get started with Beneath
---

What do you want to do?

- [Install the Beneath SDK]({{< ref "/docs/quick-starts/install-sdk" >}}). Connect to Beneath on your local machine.
- [Create a stream]({{< ref "/docs/quick-starts/create-stream" >}}). Create a stream and write data to it.
- [Read data from streams]({{< ref "/docs/quick-starts/read-from-stream" >}}). Load, replay or subscribe to data from an existing stream on Beneath.
