---
title: Read from a stream
description: A guide to loading, replaying or subscribing to data from an existing stream
menu:
  docs:
    parent: quick-starts
    weight: 400
weight: 400
---

This quick start will help you read data from a stream on Beneath.

## Install the Beneath SDK

If you haven't already, follow the [Install the Beneath SDK]({{< ref "/docs/quick-starts/install-sdk" >}}) quick start to install and authenticate Beneath on your local computer.

## Find a stream to read from

Browse the Beneath web [console](https://beneath.dev/?noredirect=1) and navigate to a stream you want to read from. In addition to streams in your own projects, there are several public streams you can explore.

## Use the API tab on the stream page

Once you've found a stream, click over to the "API" tab (next to the "Data" tab). Here you will find language-specific code snippets that you can easily copy and paste into your code environment.

If you just want to get started analyzing some data, we recommend you open a [Jupyter](https://jupyter.org/) notebook and read an entire stream into a DataFrame.
