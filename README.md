<p align="center">
  <p align="center">
    <a href="https://about.beneath.dev/?utm_source=gitlab&utm_medium=logo" target="_blank">
      <img src="https://gitlab.com/beneath-hq/beneath/-/raw/master/assets/logo/banner-icon-text-background.png" alt="Beneath" height="200">
    </a>
  </p>
</p>

<hr />

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/beneath-hq/beneath?style=flat-square)](https://goreportcard.com/report/gitlab.com/beneath-hq/beneath)
[![GoDoc](https://godoc.org/gitlab.com/beneath-hq/beneath?status.svg)](https://godoc.org/gitlab.com/beneath-hq/beneath)
[![Twitter](https://img.shields.io/badge/Follow-BeneathHQ-blue.svg?style=flat&logo=twitter)](https://twitter.com/BeneathHQ)

Beneath is a single platform for building analytics applications. Our ambition is to bundle all the useful tools and best practices in data management, so developers and data scientists can focus on models and the user experience.

Here's a list of useful links:

- Homepage: [https://about.beneath.dev](https://about.beneath.dev)
- Documentation and tutorials: [https://about.beneath.dev/docs](https://about.beneath.dev/docs)
- Ask questions, report bugs, and request features: [https://gitlab.com/beneath-hq/beneath/-/issues](https://gitlab.com/beneath-hq/beneath/-/issues)
- Client libraries: [https://gitlab.com/beneath-hq/beneath/-/tree/master/clients](https://gitlab.com/beneath-hq/beneath/-/tree/master/clients)
- Contributor guidelines: [CONTRIBUTING.md](https://gitlab.com/beneath-hq/beneath/-/blob/master/CONTRIBUTING.md)
