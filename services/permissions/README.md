# `services/permissions/`

This service manages access permissions between *owners* (users and services) and *resources* (organizations, projects and streams).

Amongst other things, it offers a permissions cache that can be used to check permissions on every request.
