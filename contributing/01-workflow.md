---
title: Workflow
description: 
menu:
  docs:
    parent: contributing
    weight: 100
weight: 100
---

## What to work on

We currently mainly expect people with a relationship to our organization to contribute, but if you're interested in contributing to Beneath, we'll welcome you with open arms! Follow these steps to get something to work on:

- First read the [License page](https://gitlab.com/beneath-hq/beneath/-/blob/master/contributing/08-license.md) to understand the current uncertainty relating to how your work will be licensed
- Browse the [Issues page](https://gitlab.com/beneath-hq/beneath/-/issues) to find or submit a problem (bugfix or feature) you want to work on
- Comment on the issue and declare your interest in helping out -- we'll then get in touch with you to coordinate your contribution

## Submitting changes

- Before getting started, read through the pages in this folder to learn more about the architecture of the codebase
- You should fork the `beneath` repository and make your changes on your own fork
- When you're done, create a merge request in this repository linking to your fork (noting the issue the merge request relates to)
- We'll review your contributions and work with you to merge it into our master branch
