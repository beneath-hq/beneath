---
title: License
description: 
menu:
  docs:
    parent: contributing
    weight: 800
weight: 800
---

The entire source code for Beneath is publicly available on [Gitlab](https://gitlab.com/beneath-hq/beneath), however, it does not currently meet the [definition of "open source"](https://opensource.org/osd).

We're pursuing an [open-core model](https://en.wikipedia.org/wiki/Open-core_model) where we offer a Community Edition and an Enterprise Edition (found in `ee/`). You can generally expect everything not in the `ee/` folder to become open source. However, at this early stage, we don't yet think we have the experience or counsel to confidently pick an open source license. We want as many people as possible to benefit from Beneath, and we also want a viable business model that allows us to continue promoting and improving Beneath. 

Thus, with the exception of the Beneath client libraries, the source code is not currently subject to any license, which means we retain the sole copyright. The [client libraries](https://gitlab.com/beneath-hq/beneath/-/tree/master/clients) are published under the MIT license and do meet the definition of "open source" – so you can freely integrate with Beneath in any software product, including in commercial software. See the [`LICENSE`](https://gitlab.com/beneath-hq/beneath/-/blob/master/LICENSE) file for details.

What does this mean for contributors? It means that if you want to contribute to the core platform, we will need you to sign a [Contributor License Agreement (CLA)](https://en.wikipedia.org/wiki/Contributor_License_Agreement). 

We're aware that the license situation is not ideal, so please don't hesitate to reach out (via the [Issues page](https://gitlab.com/beneath-hq/beneath/-/issues)) if you want to help, or have any feedback or concerns you want to share.
