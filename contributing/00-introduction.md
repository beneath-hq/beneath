---
title: Introduction
description:
menu:
  docs:
    parent: contributing
    identifier: contributing-introduction
    weight: 1
weight: 1
---

## Bug reports, questions, and feature requests

To report a bug, request a feature, or ask a question, visit/search the [Issues](https://gitlab.com/beneath-hq/beneath/-/issues) (when posting, use either the "Bug", "Feature" or "Question" description templates). We love getting your input, so don't hesitate to engage!

## Documentation for contributors

It's our ambition that all information relevant to developing the core Beneath platform is stored as close to the source code as possible, i.e., in this repository.

The documentation for contributors is structured as follows:

- This directory (`contributing/`) provides general information for contributors, including an overview of the codebase and a guideline to setting up a development environment
- The `README.md` file in each subdirectory provides implementation details about the specific module, such as its purpose, design decisions, and notes for contributors

The only files in this repository that are considered user-facing (i.e. _not_ for contributors) are:

- The documentation files in `docs/`
- The `README.md` file at the root of the repository
- The `README.md` files in the `examples/` folder
- The `README.md` files at the root of each client library directory (e.g. `clients/python/`)

## Next steps

The next pages describe the workflow around contributing changes, provide an overview of the software architecture, and a guideline on setting up a development environment
