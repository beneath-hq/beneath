/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteStreamInstance
// ====================================================

export interface DeleteStreamInstance {
  deleteStreamInstance: boolean;
}

export interface DeleteStreamInstanceVariables {
  instanceID: ControlUUID;
}
